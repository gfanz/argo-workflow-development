These are the expected steps for the Meraculous worksflow

The ConfigFile.txt contains all the parameters to run Meraculous. 
Before using check the path to files is correct and decide on the [prefix] (line 17, core assembly parameters).

These are the stages and memory loads from my last run:

                   STAGE	       TIME_IN_STAGE(s)	       PROC_TIME_SUM(s)	       PROC_TIME_MAX(s)	   PROC_TIME_SUM-MAX(s)	       PROC_MEM_MAX(kb)
       meraculous_import	                  11085	                  11080	                  10948	                  11080	                 573596
     meraculous_mercount	                  84346	                  90385	                  15438	                  16870	               79602656
     meraculous_mergraph	                  58954	                  25246	                   4572	                   4572	               32636152
          meraculous_ufx	                   4353	                  22231	                   4353	                   4353	                   4804
      meraculous_contigs	                   5019	                   5019	                   5019	                   5019	               75993832
       meraculous_bubble	                  19952	                  18940	                   6552	                  13210	               58458384
     meraculous_merblast	                  20705	                  18548	                  16589	                  18548	              119972008
          meraculous_ono	                  15157	                  15401	                   6154	                  15075	               76896680
  meraculous_gap_closure	                 150809	                 168565	                 149695	                 150761	               91549524
meraculous_final_results	                    428	                      0	                      0	                      0	                      0


This shows the most memory intensive stage was merblast (~120GB for 6 hrs), 
and the longest stages were gap_closure(~45hrs) and mercount (~25 hours), both using about 80-90 GB of memory.


To execute the run, use some form of run_meraculous.sh -c <config file>
You may need to include the path of the .sh or config files depending on setup.

There are extra command line options if you need more granularity (eg running in stages). 

See the Manual.pdf section 3.4 for more details.
