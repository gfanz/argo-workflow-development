#!/bin/bash

# This is an script used as an example to help set up orchestration using
# GFANZ docker images on open stack.

# Depends on: docker engine.

# This step would be run in the place of:
# Alignment to the reference genome with Bowtie2 (Skipped. Output = GBSv2TestData/GBS/tagsForAlign910auto.sam)
# in the tassel5_simple.sh script

# Pull the docker image for the tool bowtie2

docker pull gfanz/bowtie2

# eval statement works then: bowtie2 is aliased to `docker run --rm -v /mnt:/mnt gfanz/bowtie2'

# FIXME the eval did not work within the bash script so the alias was not set up correctly.

#eval $(docker image inspect gfanz/bowtie2 | jq -r '.[0].Config.Labels["org.label-schema.docker.cmd"]')

# Run the tool from /mnt

cd /mnt

#remove supplied sam file so we can create a new one with this script

rm /mnt/GBSV2TestData/GBS/tagsForAlign910auto.sam

# Alignment to the reference genome with Bowtie2 (Skipped. Output = GBSv2TestData/GBS/tagsForAlign910auto.sam)

# Align to reference genome

# NOTE: we could add a -p NumberOfProcessors flag to take advantage of multithreading.

docker run --rm -v /mnt:/mnt gfanz/bowtie2 --very-sensitive -x /mnt/GBSV2TestData/GBS/AGPv3/ZmB73_AGPv3 -U /mnt/GBSV2TestData/GBS/tempDir/tagsForAlign.fa.gz -S /mnt/GBSV2TestData/GBS/tagsForAlign910auto.sam
