#!/bin/bash

# This is an script used as an example to help set up orchestration using
# GFANZ docker images on open stack.

# Depends on: docker engine, jq, wget, and tar.

# Pull the docker image for the tool tassel5

docker pull gfanz/tassel5

# Set up the alias
# tassel5 is aliased to `docker run --rm -v /mnt:/mnt gfanz/tassel5'
# If the alias does not set up correctly, replace tassel5 in the script with 
# docker run --rm -v /mnt:/mnt gfanz/tassel5

# FIXME the eval did not work within the bash script so the alias was not set up correctly.

#eval $(docker image inspect gfanz/tassel5 | jq -r '.[0].Config.Labels["org.label-schema.docker.cmd"]')

# Place the data set on /mnt
cd /mnt
wget --no-check-certificate "https://data.elshiregroup.co.nz/index.php/s/RHp7w8p2AdD7eaG/download"
mv download GBSv2TestData.tar.gz

# md5sum of GBSv2TestData.tar.gz should be md5: 526d9d3a1cc8a77c75e99f88472dae25

tar -xzf GBSv2TestData.tar.gz

# Run the tool from /mnt

# GBSSeqToTagDBPlugin:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -GBSSeqToTagDBPlugin -e ApeKI -i GBSV2TestData/GBS/Chr9_10-20000000/ -db GBSv2.db -k GBSV2TestData/GBS/Pipeline_Testing_key.txt -kmerLength 64 -mnQS 20 -c 5 -endPlugin 

# TagExportToFastqPlugin:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -TagExportToFastqPlugin -db GBSv2.db -o GBSV2TestData/GBS/tempDir/tagsForAlign.fa.gz -endPlugin 

# Alignment to the reference genome with Bowtie2 (Skipped. Output = GBSv2TestData/GBS/tagsForAlign910auto.sam)

# SAMToGBSdbPlugin:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -SAMToGBSdbPlugin  -i GBSV2TestData/GBS/tagsForAlign910auto.sam -db GBSv2.db -endPlugin 

# DiscoverySNPCallerPluginV2:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -DiscoverySNPCallerPluginV2 -db GBSv2.db -sC 9 -eC 10 -mnMAF 0.01 -endPlugin 

# SNPQualityProfilerPlugin using subset of individuals:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -SNPQualityProfilerPlugin -db GBSv2.db -taxa GBSV2TestData/GBS/IBMGBSTaxaList.txt  -tname "IBM" -statFile GBSV2TestData/GBS/tempDir/SNPQualityStatsIBM.txt -endPlugin 

# SNPQualityProfilerPlugin using all individuals:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -SNPQualityProfilerPlugin -db GBSv2.db -statFile GBSV2TestData/GBS/tempDir/SNPQualityStatsAll.txt -endPlugin 

# UpdateSNPPositionQualityPlugin:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -UpdateSNPPositionQualityPlugin -db GBSv2.db -qsFile GBSV2TestData/GBS/SNPQualityScoresAll.txt -endPlugin 

# ProductionSNPCallerPluginV2:
docker run --rm -v /mnt:/mnt gfanz/tassel5 -ProductionSNPCallerPluginV2 -db GBSv2.db -e ApeKI -i GBSV2TestData/GBS/Chr9_10-20000000/ -k GBSV2TestData/GBS/Pipeline_Testing_key.txt -kmerLength 64 -minPosQS 1 -o GBSV2TestData/GBS/tempDir/maizeTestGBSGenosMinQ1.h5 -endPlugin
