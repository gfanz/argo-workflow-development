#!/bin/bash

# The purpose of this script is to create a jump server that will
# allow the user to run Argo scripts. This server is spun up inside 
# the cloud provider (e.g. catalyst cloud) infrastructure. The IP address
# of the server is in whitelisted address space. The user connects
# to the jump server via ssh and interacts with Argo.

# Usage: See README.md for details.

# Depends on Debian 10 standard cloud image

# Install byobu for ease of remote user and curl which need in this script.

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install byobu curl -y

# Install Open Stack Command Line Interface

sudo apt-get install -y python3-openstackclient python3-ceilometerclient python3-heatclient python3-neutronclient python3-swiftclient python3-octaviaclient python3-magnumclient

# Install Kubernetes Command Line (kubectl)

sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update

sudo apt-get install -y kubectl

# Install Argo Command Line 

cd /tmp

curl -sLO https://github.com/argoproj/argo/releases/download/v2.7.5/argo-linux-amd64

chmod +x argo-linux-amd64

sudo mv ./argo-linux-amd64 /usr/local/bin/argo
