# Meraculous Test Dataset for Argo Workflow Development

This directory contains 2 fastq files and a meraculous.config file. These are provided by the meraculous authors to be used to test the installation.

We will use it as a basic data set for argo workflow development. 

The files should be copied into a directory. Then the following command is issued:

> run_meraculous.sh -c meraculous.config

We will write an argo workflow to incorporate these steps and put that into this directory as well.